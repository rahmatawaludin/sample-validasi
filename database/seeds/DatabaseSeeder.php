<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
            'name' => 'doni',
            'email' => 'doni@gmail.com',
            'password' => bcrypt('rahasia')
        ]);

        DB::table('musics')->insert([
            'genre' => 'dangdut',
            'status' => 'under-review',
            'description' => 'musik rakyat'
        ]);

        DB::table('musics')->insert([
            'genre' => 'nasyid',
            'status' => 'under-review',
            'description' => 'musik positif'
        ]);

        DB::table('musics')->insert([
            'genre' => 'pop',
            'status' => 'published',
            'description' => 'musik remaja'
        ]);

        DB::table('musics')->insert([
            'genre' => 'rock',
            'status' => 'published'
        ]);
    }
}
