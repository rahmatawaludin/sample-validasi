<form method="POST" action="/songs">

    @if (count($errors) > 0)
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    {!! csrf_field() !!}

    <div>
        Track
        <input type="text" name="track_no" value="{{ old('track_no') }}">
    </div>

    <div>
        Judul
        <input type="text" name="title" class="{{ $errors->has('title') ? 'has-error' : '' }}"value="{{ old('title') }}">
        {!! $errors->first('title', '<div class="error">:message</div>') !!}
        {{-- {!! $errors->first('title', '<p>:message</p>') !!} --}}
        {{-- <p>{{ $errors->first('title') }}</p> --}}
        {{-- <ul>
            @foreach ($errors->get('title') as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul> --}}
    </div>

    <div>
        Album
        <input type="text" name="album" value="{{ old('album') }}">
    </div>

    <div>
        Penyanyi
        <input type="text" name="artist" value="{{ old('artist') }}">
        {{-- @if($errors->has('artist'))
            <p>{{ $errors->first('artist') }}</p>
        @endif --}}

        {{-- {!! $errors->first('artist', '<p>:message</p>') !!} --}}
        {!! $errors->first('artist', '<div class="error">:message</div>') !!}
        {{-- <ul>
            @foreach ($errors->get('artist') as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul> --}}
    </div>

    <div>
    Anggota
    @foreach (range(1,5) as $index)
        <p>Personil {{ $index }} : <input type="text" name="personil[{{ $index }}][name]" value="{{ old("personil.$index.name") }}"></p>
        {!! $errors->first("personil.$index.name", '<p>:message</p>') !!}
    @endforeach
    </div>

    <div>
        <button type="submit">Tambah</button>
    </div>
</form>
