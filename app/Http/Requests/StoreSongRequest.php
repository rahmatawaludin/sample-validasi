<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class StoreSongRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'title' => 'required',
            'artist' => 'required|in:Iwan Fals,Afgan|min:3',
            'album' => 'sometimes|required|alpha_num',
            'personil.*.name' => 'alpha_num'
        ];
        if ($this->has('album')) $rules['track_no'] = 'required|integer|min:0';
        return $rules;
    }

    public function messages()
    {
        return [
            'title.required' => 'Isi dengan judul lagu yang populer bro.',
            'album.required' => 'Album dari lagu ini mesti diisi ya.',
            'track_no.required' => 'Nomor tracknya diisi ya.',
            'personil.*.name.alpha_num' => 'Nama personil mesti valid.'
        ];
    }
}
