<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function testAjax(Request $request)
    {
        $this->validate($request, [
            // 'name' => 'required|alpha_num|min:5'
            // 'term_of_service' => 'accepted'
            // 'your_website' => 'active_url'
            // 'release_date' => 'date|after:10 January 2000'
            // 'name' => 'alpha'
            // 'personil' => 'array'
            // 'jumlah' => 'integer|between:10,20'
            // 'married' => 'boolean'
            // 'password' => 'confirmed'
            // 'birth_date' => 'date'
            // 'birth_date' => 'date_format:j-n-Y'
            // 'father' => 'required',
            // 'mother' => 'required|different:father'
            // 'price' => 'digits:4'
            // 'price' => 'digits_between:4,8'
            // 'client_email' => 'email'
            // 'genre' => 'exists:musics'
            // 'jenis_musik' => 'exists:musics,genre'
            // 'jenis_musik' => 'exists:musics,genre,description,NOT_NULL'
            // 'jenis_musik' => 'exists:musics,genre,status,published'
            // 'jenis_musik' => 'exists:musics,genre,status,!published'
            // 'cover' => 'image|max:2048'
            // 'artist' => 'in:Iwan Fals,Afgan'
            // 'age' => 'integer'
            // 'client_address' => 'ip'
            // 'user_property' => 'json'
            // 'name' => 'alpha_num|max:40'
            // 'attachment' => 'mimes:jpeg,png,zip|max:10240'
            // 'child' => 'regex:/d[aeiou]ni/'
            // 'genre' => 'in:pop,rock,nasyid',
            // 'reason' => 'required_if:genre,pop,rock'
            // 'genre' => 'in:pop,rock,nasyid',
            // 'reason' => 'required_unless:genre,nasyid'
            // 'song' => 'alpha_num',
            // 'album' => 'alpha_num',
            // 'artist' => 'required_with:song,album'
            // 'song' => 'alpha_num',
            // 'album' => 'alpha_num',
            // 'track_no' => 'required_with_all:song,album'
            // 'repository'
            // 'token' => 'same:123'
            // 'token' => 'alpha_num|size:8'
            // 'address' => 'string'
            // 'your_timezone' => 'timezone'
            // 'genre' => 'unique:musics'
            // 'article_link' => 'url'
            'password' => 'passcheck:' . \App\User::where('email', 'doni@gmail.com')->first()->password
        ]);

        return 'All data valid.';
    }
}
