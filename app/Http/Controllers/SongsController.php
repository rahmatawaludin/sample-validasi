<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreSongRequest;

class SongsController extends Controller
{
    public function create()
    {
        return view('songs.create');
    }

    public function store(StoreSongRequest $request)
    {
        // $rules = [
        //     'title' => 'required',
        //     'artist' => 'required|in:Iwan Fals,Afgan|min:3',
        //     'album' => 'sometimes|required|alpha_num',
        //     'personil.*.name' => 'alpha_num'
        // ];

        // if ($request->has('album')) $rules['track_no'] = 'required|integer|min:0';

        // $this->validate($request, $rules, [
        //     'title.required' => 'Isi dengan judul lagu yang populer bro.',
        //     'album.required' => 'Album dari lagu ini mesti diisi ya.',
        //     'track_no.required' => 'Nomor tracknya diisi ya.',
        //     'personil.*.name.alpha_num' => 'Nama personil mesti valid.'
        // ]);

        \App::setLocale('id');

        return 'Lagu ' . $request->title . ' oleh ' . $request->artist . ' telah disimpan.';
    }
}

